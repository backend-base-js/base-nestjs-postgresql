FROM node:12.19.0-alpine3.9 as dev

WORKDIR /usr/src/app

COPY package*.json ./

RUN apk update && apk add g++ make python openssl

RUN npm install glob rimraf

RUN npm install