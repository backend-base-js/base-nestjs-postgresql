# Start
1. docker-compose up -d
2. npm install
3. npm run start:dev or npm run start:debug

# Script more
1. npm run format ( format source code )

# Source base
```bash
src
  - common
    - constants
    - decorators
    - enums
    - errors
    - interfaces
    - validations
  - config ( all config environment )
  - entities
  - guard
  - i18
  - modules ( service, controller, dtos of one module )
  - utils
```