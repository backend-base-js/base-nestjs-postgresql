import * as dotenv from 'dotenv';
dotenv.config({ override: true });

export default {
  info: {
    name: process.env.INFO_NAME || 'Nest App',
    port: Number(process.env.INFO_PORT) || 1804,
    prefix: process.env.INFO_PREFIX || 'api',
    description: process.env.INFO_DESCRIPTION || 'Nest App',
    version: process.env.INFO_VERSION || '1.0',
  },
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY || 'nest-app',
    accessTokenExpires: Number(process.env.JWT_ACCESS_TOKEN_EXPIRES) || 60 * 10,
    refreshTokenExpires: Number(process.env.JWT_REFRESH_TOKEN_EXPIRES) || 60 * 60 * 24 * 30,
  },
  database: {
    username: process.env.POSTGRES_USER || 'postgres',
    password: process.env.POSTGRES_PASSWORD || 'postgres',
    databaseName: process.env.POSTGRES_DB || 'nestjs-postgresql',
    host: process.env.POSTGRES_HOST || 'localhost',
    port: Number(process.env.POSTGRES_PORT),
  },
  cloudinary: {
    url: process.env.CLOUDINARY_URL,
    cloudName: process.env.CLOUD_NAME,
    apiKey: process.env.API_KEY,
    apiSecret: process.env.API_SECRET,
    domain: process.env.CLOUDINARY_DOMAIN,
  },
};
