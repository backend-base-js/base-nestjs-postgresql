import config from '@config';

export class Utils {
  static RegexConstants = class {
    static REGEX_EMAIL =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    static REGEX_NUMBER = /^\+?([1-9]\d*)$/;

    //eslint-disable-next-line
    static REGEX_PHONE = /^[0-9\+]{10,12}$/;
  };

  /**
   *
   * @param input string
   * @returns boolean
   */
  static isEmail(input: string): boolean {
    const re = new RegExp(this.RegexConstants.REGEX_EMAIL);
    return re.test(input);
  }

  /**
   *
   * @param input string
   * @returns boolean
   */
  static isNumber(input: string): boolean {
    const re = new RegExp(this.RegexConstants.REGEX_NUMBER);
    return re.test(input);
  }

  /**
   * replace &0, &1,... to input values ß
   * @param originSrc
   * @param args
   * @returns
   */
  static printString(originSrc: string, args: string[] = []): string {
    if (args && args.length > 0) {
      return originSrc.replace(new RegExp('([\\&\\d+]+)', 'g'), function (_unused, index) {
        return args[index.replace('&', '')];
      });
    } else {
      return originSrc;
    }
  }

  /**
   *
   * @param data [any[], number]
   * @param options { page: number; limit: number }
   * @returns metadata
   */
  static paginateResponse(
    data: [any[], number],
    options: { page: number; limit: number },
  ): { data: any; metadata: any } {
    const { page, limit } = options;
    const result = data?.[0];
    // record total
    let total = data?.[1];

    if (total > 100) {
      total = 100;
    }

    // record number on a page
    let itemPage = 0;
    // current page number
    let currentPage = 0;
    // last page number
    let lastPage = 0;
    if (result && page <= 0) {
      // get all
      itemPage = result.length;
      currentPage = 1;
    } else if (result && result.length) {
      // get by paginate and have data
      lastPage = Math.ceil(total / limit);
      itemPage = result.length;
      currentPage = page;
    } else {
      // get by paginate and do data
      total = 0;
      currentPage = page;
    }

    if (lastPage > 10) {
      lastPage = 10;
      total = lastPage * limit;
    }

    return {
      data: [...result],
      metadata: {
        total: total,
        itemPage: itemPage,
        currentPage: currentPage,
        lastPage: lastPage,
      },
    };
  }

  /**
   *
   * @param object
   * @param fields
   * @returns
   */
  static omit = (object: any, fields: string[]) => {
    const keys = Object.keys(object);
    keys.forEach((item) => {
      if (fields.includes(item)) {
        delete object[item];
      }
    });
    return object;
  };

  /**
   *
   * @param givenString string
   * @returns boolean
   */
  static isContainNumber(givenString: string) {
    return /\d/.test(givenString);
  }

  /**
   *
   * @param signature string
   */
  static getSignUrlUploadCloudinary(signature: string) {
    const cloudinaryConfig = config.cloudinary;
    return `${cloudinaryConfig.domain}${cloudinaryConfig.cloudName}/upload?api_key=${cloudinaryConfig.apiKey}&signature=${signature}`;
  }
}
