import { ApiResponseProperty } from '@nestjs/swagger';
import { ERROR_INFO } from '@common/constants';
import * as bcrypt from 'bcrypt';

export class MessageResponse {
  @ApiResponseProperty()
  code: number;
  @ApiResponseProperty()
  info: string;
}

export class Metadata {
  @ApiResponseProperty()
  total?: number;
  @ApiResponseProperty()
  itemPage?: number;
  @ApiResponseProperty()
  currentPage?: number;
  @ApiResponseProperty()
  lastPage?: number;
}

/**
 * create common response object for application
 * @param content
 * @param metadata
 * @returns
 */
export function responseHelper<T>(
  content: T,
  metadata?: Metadata,
  code?: number,
  info?: string,
): MessageResponse {
  // handle calculator total page
  const totalPage = handleMetaData(metadata);

  const res = {
    code: 200,
    info: ERROR_INFO.SUCCESS,
    content: null,
    metadata: undefined,
  };

  res.content = content;
  res.metadata = metadata
    ? {
        ...metadata,
        totalPage: totalPage,
      }
    : undefined;

  if (code) {
    res.code = code;
  }
  if (info) {
    res.info = info;
  }
  return res;
}

/**
 *
 * @param metaData Metadata
 * @returns number
 */
export function handleMetaData(metaData: Metadata) {
  let totalPage = 1;
  const totalItem = metaData?.total;
  const itemPage = metaData?.itemPage;

  if (totalItem && itemPage) {
    const checkData = totalItem % itemPage;
    const pageTerm = totalItem / itemPage;
    if (checkData === 0) {
      totalPage = pageTerm;
    } else {
      totalPage = Math.floor(pageTerm) + 1;
    }
  }
  return totalPage;
}

/**
 * compare input password during login
 * @param orgPassword
 * @param encryptedPassword
 * @returns
 */
export function comparePassword(orgPassword: string, encryptedPassword: string) {
  const isPassword = bcrypt.compareSync(orgPassword, encryptedPassword);
  return isPassword;
}
