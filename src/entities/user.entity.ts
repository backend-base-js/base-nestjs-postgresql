import { RoleType } from '@common/enums';
import { Entity, Column, BeforeInsert } from 'typeorm';
import { CommonEntity } from './common.entity';
import { genSaltSync, hashSync } from 'bcrypt';
import { SALT_ROUND } from '@common/constants/common.const';

@Entity({ name: 'user' })
export class UserEntity extends CommonEntity {
  @Column({ unique: true, type: 'varchar', length: 100 })
  public username: string;

  @Column({ unique: true, type: 'varchar', length: 100 })
  public email: string;

  @Column({ type: 'varchar', length: 100 })
  public password: string;

  @Column({ type: 'varchar', default: RoleType.END_USER })
  public role: RoleType;

  @BeforeInsert()
  hashPassword() {
    if (this.password) {
      this.password = hashSync(this.password, genSaltSync(SALT_ROUND));
    }
  }
}
