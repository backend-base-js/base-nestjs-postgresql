import { Column, Entity } from 'typeorm';
import { CommonEntity } from './common.entity';

@Entity({ name: 'file' })
export class FileEntity extends CommonEntity {
  @Column({ type: 'varchar' })
  public url: string;

  @Column({ type: 'varchar', unique: true })
  public publicId: string;

  @Column({ type: 'jsonb', nullable: true })
  public metadata: Record<string, string>;
}
