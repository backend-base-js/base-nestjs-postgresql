import { Column, Entity } from 'typeorm';
import { CommonEntity } from './common.entity';

@Entity({ name: 'contact' })
export class ContactEntity extends CommonEntity {
  @Column({ type: 'varchar', length: 100 })
  public name: string;

  @Column({ type: 'varchar', length: 100 })
  public phone: string;

  @Column({ type: 'varchar' })
  public address: string;
}
