import { Injectable, ValidationPipe } from '@nestjs/common';
import { errorFactory } from '../errors';

@Injectable()
export class ValidationBodyPipe extends ValidationPipe {
  constructor() {
    super({
      transform: true,
    });
  }
  exceptionFactory = errorFactory([]);
}
