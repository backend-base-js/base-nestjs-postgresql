import { PAGINATION_DEFAULT } from '@common/constants';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export interface IMedia {
  path: string;
  fileName: string;
  fileSize: string;
}

export interface ITokenContent {
  id?: number;
}

export class ITokenInfo {
  @ApiProperty({ required: true })
  accessToken: string;

  @ApiProperty({ required: true })
  refreshToken: string;

  @ApiProperty({ required: true })
  expireTime: number;
}

export class IParamsCommonReq {
  @ApiProperty({ default: PAGINATION_DEFAULT.LIMIT })
  @Type(() => Number)
  limit?: number;

  @ApiProperty({ default: PAGINATION_DEFAULT.PAGE })
  @Type(() => Number)
  page?: number;
}

export class IAccountInfo {
  id: number;
  iat: number;
  exp: number;
}
