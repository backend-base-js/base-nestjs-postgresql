export enum RoleType {
  ADMIN = 'ADMIN',
  SUPPER_ADMIN = 'SUPPER_ADMIN',
  END_USER = 'END_USER',
}

export enum OrderType {
  DESC = 'DESC',
  ASC = 'ASC',
}

export enum SystemType {
  CMS_SYSTEM = 'CMS_SYSTEM',
  CLIENT_SYSTEM = 'CLIENT_SYSTEM',
}
