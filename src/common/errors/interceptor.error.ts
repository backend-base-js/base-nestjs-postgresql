import {
  CallHandler,
  ExecutionContext,
  HttpException,
  HttpStatus,
  InternalServerErrorException,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, catchError, throwError } from 'rxjs';
import { ErrorObject } from '../errors';
import { getI18nContextFromArgumentsHost, I18nContext } from 'nestjs-i18n';
import { ERROR_INFO } from '../constants';
import { responseHelper, Utils } from 'src/utils';
import { ErrorMessageKey } from './messages';

/**
 * handle any error in the system
 */
export class InterceptorErrors implements NestInterceptor {
  intercept(_context: ExecutionContext, next: CallHandler<any>): Observable<any> {
    return next.handle().pipe(
      catchError((err) => {
        //access I18nContext from ArgumentHost
        const i18n = getI18nContextFromArgumentsHost(_context);
        if (err.isError) {
          //replace message key to message value
          err.messageContent = i18n.t(err.messageKey);
          return throwError(
            () =>
              new HttpException(
                responseHelper(
                  Utils.omit(err, ['isError', 'messageKey']),
                  undefined,
                  err.statusCode,
                  ERROR_INFO.FAIL,
                ),
                err.statusCode,
              ),
          );
        } else if (err instanceof HttpException) {
          return throwError(
            () =>
              new HttpException(
                responseHelper(err.message, undefined, err.getStatus(), ERROR_INFO.FAIL),
                err.getStatus(),
              ),
          );
        } else {
          Logger.error(`System Error: ${err.toString()}`);
          return throwError(
            () =>
              new InternalServerErrorException(
                responseHelper(
                  ErrorMessageKey.SERVER_ERROR,
                  undefined,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                  ERROR_INFO.FAIL,
                ),
              ),
          );
        }
      }),
    );
  }

  /**
   * multiple language for constraints in validation
   * @param constraints
   * @param i18n
   */
  private i18nMessageForConstrains(constraints: ErrorObject[], i18n: I18nContext) {
    let message: string[] = [];
    if (constraints) {
      for (const constraint of constraints) {
        message = [];
        constraint.message.map((item) => {
          const arrItem = item.split('|');
          let content = i18n.t<string>(arrItem[0]);
          content = content.replace('$property', constraint.property);
          const argument = JSON.parse(arrItem[1]) as {
            value: string;
            constraints: string[];
          };
          if (argument.constraints) {
            content = Utils.printString(content, argument.constraints);
          }
          message.push(content);
        });
        constraint.message = message;
      }
    }
  }
}
