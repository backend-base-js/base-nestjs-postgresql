import { ValidationError } from '@nestjs/common';
import { ERROR_RES, ERROR_TYPE } from '../constants';
import { ErrorMessageKey } from './messages';

export class ErrorObject {
  property: string;
  message: string[];
}

export class BaseError extends Error {
  protected readonly isError: boolean = true;
  statusCode: number;
  messageCode: string;
  name: string;
  errorType: ERROR_TYPE = ERROR_TYPE.BUSINESS;
  constraints: ErrorObject[];
  messageKey: string;

  constructor(messageKey: string, name: string, statusCode: number, constraints?: ErrorObject[]) {
    super(messageKey);
    this.messageKey = messageKey;
    this.name = name;
    this.statusCode = statusCode;
    this.constraints = constraints;
    Object.setPrototypeOf(this, new.target.prototype);
    Error.captureStackTrace(this);
  }
}

export class InputValidationError extends BaseError {
  errorType: ERROR_TYPE = ERROR_TYPE.VALIDATION;
  constructor(message: string, constraints: ErrorObject[]) {
    super(
      message,
      ERROR_RES['VALIDATION_ERROR'].name,
      ERROR_RES['VALIDATION_ERROR'].statusCode,
      constraints,
    );
  }
}

// validation
export const errorFactory = (constraints: ErrorObject[]) => {
  return (errors: ValidationError[]) => {
    constraints = [];
    errors.forEach((item) => {
      const objErr = new ErrorObject();
      objErr.property = item.property;
      objErr.message = [];
      for (const key of Object.keys(item.constraints)) {
        objErr.message.push(item.constraints[key]);
      }
      constraints.push(objErr);
    });
    return new InputValidationError(ErrorMessageKey.REQUEST_BODY_VALIDATION, constraints);
  };
};
export class ServerError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['INTERNAL_ERROR'].name,
      ERROR_RES['INTERNAL_ERROR'].statusCode,
      constraints,
    );
  }
}
export class BadRequestError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['BAD_REQUEST_ERROR'].name,
      ERROR_RES['BAD_REQUEST_ERROR'].statusCode,
      constraints,
    );
  }
}
export class NotFoundError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['NOT_FOUND_ERROR'].name,
      ERROR_RES['NOT_FOUND_ERROR'].statusCode,
      constraints,
    );
  }
}
export class ConflictError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['CONFLICT_ERROR'].name,
      ERROR_RES['CONFLICT_ERROR'].statusCode,
      constraints,
    );
  }
}

export class ForbiddenError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['FORBIDDEN_ERROR'].name,
      ERROR_RES['FORBIDDEN_ERROR'].statusCode,
      constraints,
    );
  }
}
export class UnauthorizedError extends BaseError {
  constructor(messageKey: string, constraints?: ErrorObject[]) {
    super(
      messageKey,
      ERROR_RES['UNAUTHORIZED_ERROR'].name,
      ERROR_RES['UNAUTHORIZED_ERROR'].statusCode,
      constraints,
    );
  }
}
