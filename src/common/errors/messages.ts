export enum ErrorMessageKey {
  SERVER_ERROR = 'SERVER_ERROR',
  USER_NOT_FOUND = 'USER_NOT_FOUND',
  USERNAME_REQUIRED = 'USERNAME_REQUIRED',
  PASSWORD_REQUIRED = 'PASSWORD_REQUIRED',
  EMAIL_REQUIRED = 'EMAIL_REQUIRED',
  USERNAME_VALIDATED = 'USERNAME_VALIDATED',
  EMAIL_VALIDATED = 'EMAIL_VALIDATED',
  USERNAME_OR_EMAIL_VALID = 'USERNAME_OR_EMAIL_VALID',
  PASSWORD_INCORRECT = 'PASSWORD_INCORRECT',
  UNAUTHORIZED = 'UNAUTHORIZED',
  REQUEST_BODY_VALIDATION = 'REQUEST_BODY_VALIDATION',
  CONTACT_NOT_FOUND = 'CONTACT_NOT_FOUND',
  PERMISSION_DENIED = 'PERMISSION_DENIED',
}
