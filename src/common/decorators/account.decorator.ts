import { IAccountInfo } from '@common/interfaces';
import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const AccountInfo = createParamDecorator((_data: any, context: ExecutionContext) => {
  const req = context.switchToHttp().getRequest();
  const accountInfo: IAccountInfo = req['account'];
  return accountInfo;
});
