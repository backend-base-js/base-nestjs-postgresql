export const SALT_ROUND = 12;

export const PAGINATION_DEFAULT = {
  LIMIT: 10,
  PAGE: 1,
};

export const initCloudinaryFolder = {
  temp: 'temp',
  assets: 'assets',
};
