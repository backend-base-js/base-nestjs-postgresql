import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import helmet from 'helmet';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import config from '@config';
import { InterceptorErrors } from '@common/errors/interceptor.error';
import { ValidationBodyPipe } from '@common/validations';
import * as cloudinary from 'cloudinary';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(`/${config.info.version}/${config.info.prefix}`);

  //use interceptor to show error
  app.useGlobalInterceptors(new InterceptorErrors());
  // validation
  app.useGlobalPipes(new ValidationBodyPipe());

  app.enableCors(); //enable cors
  app.use(helmet());

  // cloudinary config
  cloudinary.v2.config({
    cloud_name: String(config.cloudinary.cloudName),
    api_key: String(config.cloudinary.apiKey),
    api_secret: String(config.cloudinary.apiSecret),
  });

  // swagger setup
  const configSwagger = new DocumentBuilder()
    .setTitle(config.info.name)
    .setDescription(config.info.description)
    .setVersion(config.info.version)
    .build();
  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup(`${config.info.version}/${config.info.prefix}/specs`, app, document);

  // run app
  await app.listen(config.info.port);
}
bootstrap()
  .then(() => {
    Logger.log(
      `Swagger for ${config.info.name}: http://localhost:${config.info.port}/${config.info.version}/${config.info.prefix}/specs`,
    );
    console.log(
      `${config.info.name} is running on port: http://localhost:${config.info.port}/${config.info.version}/${config.info.prefix}`,
    );
  })
  .catch((err) => {
    console.error(err);
  });
