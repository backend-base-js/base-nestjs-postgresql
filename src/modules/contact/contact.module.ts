import { Module } from '@nestjs/common';
import { ContactController } from './contact.controller';
import { ContactService } from './contact.service';
// import { AuthenticationModule } from '../authentication/authentication.module';
// import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [],
  controllers: [ContactController],
  providers: [ContactService],
  exports: [],
})
export class ContactModule {}
