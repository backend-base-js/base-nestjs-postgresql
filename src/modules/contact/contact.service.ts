import { OrderType } from '@common/enums';
import { NotFoundError } from '@common/errors';
import { ErrorMessageKey } from '@common/errors/messages';
import { ContactEntity } from '@entities';
import { Inject, Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, EntityManager } from 'typeorm';
import { Logger } from 'winston';
import { IContactBodyReq, IFindContactReq } from './dtos/contact.req';
import { IListContactRes } from './dtos/contact.res';

@Injectable()
export class ContactService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   *
   * @param params IFindContactReq
   * @returns IListContactRes
   */
  async findAll(params?: IFindContactReq): Promise<IListContactRes> {
    let responseData: IListContactRes | null = null;
    try {
      const limit = params?.limit;
      const page = params.page - 1;

      await this.connection.transaction(async (manager: EntityManager) => {
        // get total record
        const $totalQuery = manager.getRepository(ContactEntity).createQueryBuilder('contact');

        if (params?.name) {
          $totalQuery.orWhere('contact.name like :name', {
            name: `%${params.name ? params.name : ''}%`,
          });
        }

        if (params.phone) {
          $totalQuery.orWhere('contact.phone like :phone', {
            phone: `%${params.phone || ''}%`,
          });
        }
        const totalRecord = await $totalQuery.getCount();

        // query data
        const $queries = manager.getRepository(ContactEntity).createQueryBuilder('contact');

        if (params?.name) {
          $queries.orWhere('contact.name like :name', {
            name: `%${params.name ? params.name : ''}%`,
          });
        }
        if (params.phone) {
          $queries.orWhere('contact.phone like :phone', {
            phone: `%${params.phone ? params.phone : ''}%`,
          });
        }

        if (limit) {
          $queries.take(limit);
        }
        if (page) {
          $queries.skip(page * limit);
        }

        const listContact = await $queries.orderBy('contact.createdAt', OrderType.DESC).getMany();
        responseData = {
          data: listContact,
          total: totalRecord,
          currentPage: page + 1,
          limit: limit,
        };
      });
      return responseData;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param body IContactBodyReq
   * @returns ContactEntity
   */
  async createContact(body: IContactBodyReq): Promise<ContactEntity> {
    let contactRes: ContactEntity | null = null;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        const newContact = manager.create(ContactEntity, {
          name: body.name,
          phone: body.phone,
          address: body.address,
        });
        const createNewContact = await manager.save(newContact);
        contactRes = createNewContact;
      });
      return contactRes;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param id number
   * @param body IContactBodyReq
   */
  async updateContact(id: number, body: IContactBodyReq) {
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        // check contact valid
        const contactItem = await manager.findOne(ContactEntity, { where: { id } });
        if (!contactItem) throw new NotFoundError(ErrorMessageKey.CONTACT_NOT_FOUND);

        await manager.update(ContactEntity, contactItem.id, {
          ...body,
        });
      });
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param id number
   * @returns ContactEntity
   */
  async findById(id: number): Promise<ContactEntity> {
    let contactRes: ContactEntity | null = null;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        // check contact valid
        const contactItem = await manager.findOne(ContactEntity, { where: { id } });
        if (!contactItem) throw new NotFoundError(ErrorMessageKey.CONTACT_NOT_FOUND);
        contactRes = contactItem;
      });
      return contactRes;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param id number
   * @returns ContactEntity
   */
  async removeItem(id: number): Promise<ContactEntity> {
    let contactRes: ContactEntity | null = null;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        // check contact valid
        const contactItem = await manager.findOne(ContactEntity, { where: { id } });
        if (!contactItem) throw new NotFoundError(ErrorMessageKey.CONTACT_NOT_FOUND);
        await manager.remove(ContactEntity, contactItem);
        contactRes = { ...contactItem, id };
      });
      return contactRes;
    } catch (error) {
      throw error;
    }
  }
}
