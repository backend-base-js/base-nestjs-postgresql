import {
  Controller,
  Get,
  Delete,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  UseGuards,
  Body,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { Logger } from 'winston';
import { ContactService } from './contact.service';
import { JwtAuthenticationGuard } from '@guard';
import { IContactBodyReq, IFindContactReq } from './dtos/contact.req';
import { responseHelper } from '@utils';
import { ContactEntity } from '@entities';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { IResDeleteContact } from './dtos/contact.res';

@ApiTags('Contact')
@Controller('contact')
export class ContactController {
  constructor(
    private readonly contactService: ContactService,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   *
   * @param params IFindUserReq
   * @returns IListUserRes
   */
  @ApiBearerAuth()
  @ApiParam({
    name: 'limit',
    type: 'number',
    required: false,
  })
  @ApiParam({
    name: 'page',
    type: 'number',
    required: false,
  })
  @ApiParam({
    name: 'name',
    type: 'string',
    required: false,
  })
  @ApiParam({
    name: 'phone',
    type: 'string',
    required: false,
  })
  @ApiOperation({
    operationId: 'get list contact',
    description: 'Get list contact',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: [ContactEntity],
  })
  @Get('/list')
  @HttpCode(200)
  @UseGuards(JwtAuthenticationGuard)
  async getContacts(@Query() params?: IFindContactReq) {
    const contacts = await this.contactService.findAll(params);
    return responseHelper<ContactEntity[]>(contacts.data, {
      total: contacts?.total,
      currentPage: contacts?.currentPage,
      itemPage: contacts.limit,
    });
  }

  @ApiBearerAuth()
  @ApiOperation({
    operationId: 'get detail contact',
    description: 'Get detail contact',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: ContactEntity,
  })
  @Get('/:id')
  @HttpCode(200)
  @UseGuards(JwtAuthenticationGuard)
  async getContactById(@Param('id') id: number) {
    const contact = await this.contactService.findById(id);
    return responseHelper<ContactEntity>(contact);
  }

  @ApiBearerAuth()
  @ApiOperation({
    operationId: 'remove contact',
    description: 'Remove contact',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: IResDeleteContact,
  })
  @Delete('/:id')
  @HttpCode(200)
  async removeContact(@Param('id') id: number) {
    const res = await this.contactService.removeItem(id);
    return responseHelper<IResDeleteContact>({ id: res?.id });
  }

  @ApiBearerAuth()
  @ApiOperation({
    operationId: 'create new contact',
    description: 'Create new contact',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: ContactEntity,
  })
  @Post()
  @HttpCode(200)
  async createNewContact(@Body() body: IContactBodyReq) {
    const contact = await this.contactService.createContact(body);
    return responseHelper<ContactEntity>(contact);
  }

  @ApiBearerAuth()
  @ApiOperation({
    operationId: 'update contact',
    description: 'Update contact',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: String,
  })
  @Put('/:id')
  @HttpCode(200)
  async updateContact(@Param('id') id: number, @Body() body: IContactBodyReq) {
    await this.contactService.updateContact(id, body);
    return responseHelper<string>('Update success');
  }
}
