import { ContactEntity } from '@entities';
import { ApiProperty } from '@nestjs/swagger';

export class IListContactRes {
  @ApiProperty({ required: true })
  data?: ContactEntity[];

  @ApiProperty({ required: true })
  total?: number;

  @ApiProperty({ required: true })
  currentPage?: number;

  @ApiProperty({ required: true })
  limit?: number;
}

export class IResDeleteContact {
  @ApiProperty()
  id?: number;
}
