import { IParamsCommonReq } from '@common/interfaces';
import { ApiProperty } from '@nestjs/swagger';

export class IContactBodyReq {
  @ApiProperty()
  name: string;

  @ApiProperty()
  phone: string;

  @ApiProperty()
  address: string;
}

export class IFindContactReq extends IParamsCommonReq {
  @ApiProperty({ nullable: true })
  name?: string;

  @ApiProperty({ nullable: true })
  phone?: string;
}
