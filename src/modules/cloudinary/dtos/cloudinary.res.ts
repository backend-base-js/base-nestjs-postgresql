export class ICloudinarySignUrlRes {
  url: string;
}

export class SignUrlRes {
  url: string;
  publicId: string;
}
