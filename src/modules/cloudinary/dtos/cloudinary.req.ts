export class ICloudinaryParams {
  fileName: string;
  folderName?: string;
}

export class ICloudinaryMoveFileParams {
  oldPublicId: string;
  newFolder?: string;
}
