import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  Query,
  Post,
  Put,
  Delete,
  Body,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { Logger } from 'winston';
import { CloudinaryService } from './cloudinary.service';
import { ICloudinarySignUrlRes } from './dtos/cloudinary.res';

@ApiTags('Cloudinary')
@Controller('cloudinary')
export class CloudinaryController {
  constructor(
    private readonly cloudinaryService: CloudinaryService,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   *
   * @param params IFindUserReq
   * @returns IListUserRes
   */
  @ApiBearerAuth()
  @ApiQuery({
    name: 'fileName',
    type: 'string',
    required: true,
  })
  @ApiQuery({
    name: 'folder',
    type: 'string',
    required: false,
  })
  @ApiOperation({
    operationId: 'get url upload',
    description: 'Get url upload',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: ICloudinarySignUrlRes,
  })
  @Get('/sign-url')
  @HttpCode(200)
  async signUrlUpload(@Query() params: { fileName: string; folder?: string }) {
    return this.cloudinaryService.signUrl(params);
  }

  @Post('/upload-url')
  @HttpCode(200)
  async uploadRemoteFile(@Body() body: { urls: string[]; folder?: string }) {
    return this.cloudinaryService.uploadRemoteFiles(body.urls, body.folder);
  }

  @Put('/move-file')
  @HttpCode(200)
  async moveFiles(@Body() body: { publicIds: string[]; folder?: string }) {
    return this.cloudinaryService.moveFiles(body.publicIds, body.folder);
  }

  @Delete('/delete')
  @HttpCode(200)
  async delete(@Body() params: { publicIds: string[] }) {
    return this.cloudinaryService.delete(params.publicIds);
  }
}
