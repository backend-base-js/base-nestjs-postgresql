import { Module } from '@nestjs/common';
import { CloudinaryController } from './cloudinary.controller';
import { CloudinaryService } from './cloudinary.service';
import { v2 } from 'cloudinary';
import { ConfigService } from '@nestjs/config';
import { FileModule } from '@modules/file/file.module';

@Module({
  imports: [FileModule],
  controllers: [CloudinaryController],
  providers: [
    CloudinaryService,
    {
      provide: 'CLOUDINARY',
      useFactory: () => {
        const configService = new ConfigService();
        return v2.config({
          cloud_name: configService.get('CLOUD_NAME'),
          api_key: configService.get('API_KEY'),
          api_secret: configService.get('API_SECRET'),
          cloudinary_domain: configService.get('CLOUDINARY_DOMAIN'),
        });
      },
    },
  ],
})
export class CloudinaryModule {}
