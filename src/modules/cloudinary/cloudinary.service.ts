import { initCloudinaryFolder } from '@common/constants';
import { ServerError } from '@common/errors';
import { FileService } from '@modules/file/file.service';
import { Injectable } from '@nestjs/common';
import { v2 } from 'cloudinary';
import { format } from 'date-fns';
import * as slug from 'slug';
import { SignUrlRes } from './dtos/cloudinary.res';
import { BadRequestError } from '../../common/errors/index';

@Injectable()
export class CloudinaryService {
  constructor(private readonly fileService: FileService) {}

  /**
   *
   * @param args SignUrlReq
   * @returns SignUrlRes
   */
  signUrl(args: { fileName: string; folder?: string }): SignUrlRes {
    const folderName = args.folder || initCloudinaryFolder.temp;
    const fileName = `${format(new Date(), 'yyyyMMddHHmmss')}-${slug(args.fileName)}`;

    const publicId = `${folderName}/${fileName}`;
    const timestamp = Math.round(new Date().getTime() / 1000);

    const signatureData = v2.utils.sign_request({
      timestamp,
      public_id: publicId,
    });

    if (!signatureData || Object.values(signatureData).length <= 0)
      throw new ServerError('Get sign url error.');

    const signUrlUpload = this.getUrlUpload({ ...signatureData });

    return {
      url: signUrlUpload,
      publicId,
    };
  }

  getUrlUpload(params: { [key: string]: any; signature: string; api_key: string }) {
    const { cloud_name, api_key, cloudinary_domain } = v2.config();
    return `${cloudinary_domain}${cloud_name}/upload?api_key=${api_key}&signature=${params.signature}&timestamp=${params.timestamp}&public_id=${params.public_id}`;
  }

  /**
   *
   * @param publicIds string[]
   * @param folder string | undefined
   * @returns File[]
   */
  async moveFiles(publicIds: string[], folder?: string) {
    const newFolder = folder || initCloudinaryFolder.assets;
    if (!publicIds) throw new BadRequestError('Public id is required.');

    const result = await Promise.all(
      publicIds.map(async (item) => {
        const splitName = item.split('/');
        const fileName = splitName[splitName.length - 1];
        const newPublicId = `${newFolder}/${fileName}`;

        const newCloudinaryFile = await v2.uploader.rename(item, newPublicId);

        // check insert or update to database
        const fileValid = await this.fileService.findFirst({
          where: { publicId: newPublicId },
        });

        if (fileValid) {
          return await this.fileService.update({
            publicId: newCloudinaryFile.public_id,
            url: newCloudinaryFile.url,
          });
        }

        // insert file
        return await this.fileService.create({
          url: newCloudinaryFile.url,
          publicId: newCloudinaryFile.public_id,
          metadata: {
            format: newCloudinaryFile.format,
            resource_type: newCloudinaryFile.resource_type,
            bytes: newCloudinaryFile.bytes,
            height: newCloudinaryFile.height,
            width: newCloudinaryFile.width,
          },
        });
      }),
    );

    return result;
  }

  /**
   *
   * @param publicIds string[]
   * @returns File[]
   */
  async delete(publicIds: string[]) {
    const result = await Promise.all(
      publicIds.map(async (item) => {
        // call cloudinary delete and database
        const fileCloudinaryDelete = await v2.uploader.destroy(item);
        if (fileCloudinaryDelete.result === 'ok') {
          return await this.fileService.delete({ publicId: item });
        }
      }),
    );
    return result;
  }

  /**
   *
   * @param urls string[]
   * @param folder string | undefined
   * @returns File[]
   */
  async uploadRemoteFiles(urls: string[], folder?: string) {
    const folderName = folder || initCloudinaryFolder.assets;
    const timestamp = Math.round(new Date().getTime() / 1000);

    const results = await Promise.all(
      urls.map(async (item) => {
        const imageUpload = await v2.uploader.upload(item, {
          timestamp,
          folder: folderName,
        });

        // insert file
        return await this.fileService.create({
          url: imageUpload.url,
          publicId: imageUpload.public_id,
          metadata: {
            format: imageUpload.format,
            resource_type: imageUpload.resource_type,
            bytes: imageUpload.bytes,
            height: imageUpload.height,
            width: imageUpload.width,
          },
        });
      }),
    );

    return results;
  }
}
