import { RoleType } from '@common/enums';
import { ErrorMessageKey } from '@common/errors/messages';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ISignInReq {
  @ApiProperty({ required: true })
  @IsNotEmpty({
    message: ErrorMessageKey.USERNAME_REQUIRED,
  })
  username: string;

  @ApiProperty({ required: true })
  @IsNotEmpty({
    message: ErrorMessageKey.PASSWORD_REQUIRED,
  })
  password: string;

  @ApiProperty({ required: false })
  systemType?: string;
}

export class ISignUpReq {
  @ApiProperty({ required: true })
  username: string;

  @ApiProperty({ required: true })
  email: string;

  @ApiProperty({ required: true })
  password: string;

  @ApiProperty()
  role?: RoleType;
}
