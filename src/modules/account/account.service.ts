import { RoleType } from '@common/enums';
import { InputValidationError, UnauthorizedError } from '@common/errors';
import { ITokenContent, ITokenInfo } from '@common/interfaces';
import { UserEntity } from '@entities';
import { AuthenticationService } from '@modules/authentication/authentication.service';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectConnection } from '@nestjs/typeorm';
import { comparePassword } from '@utils/helper';
import { Connection, EntityManager } from 'typeorm';
import { Logger } from 'winston';
import { SystemType } from '../../common/enums/entity.enum';
import { ErrorMessageKey } from './../../common/errors/messages';
import { ISignInReq, ISignUpReq } from './dtos/account.req';

@Injectable()
export class AccountService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    private readonly authenticationServices: AuthenticationService,
    private readonly configService: ConfigService,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   * login user into system
   * @param body ISignInReq
   * @returns ITokenInfo
   */
  async signIn(body: ISignInReq): Promise<ITokenInfo | null> {
    let tokenInfo: ITokenInfo | null = null;
    const systemType = body?.systemType || SystemType.CLIENT_SYSTEM;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        if (!body.username)
          throw new InputValidationError(ErrorMessageKey.USERNAME_REQUIRED, [
            { message: [ErrorMessageKey.USERNAME_REQUIRED], property: 'username' },
          ]);
        if (!body.password)
          throw new InputValidationError(ErrorMessageKey.PASSWORD_REQUIRED, [
            { message: [ErrorMessageKey.PASSWORD_REQUIRED], property: 'password' },
          ]);

        // check user valid
        const userCheck = await manager.findOne(UserEntity, {
          where: [{ username: body.username }, { email: body.username }],
        });

        if (!userCheck)
          throw new InputValidationError(ErrorMessageKey.USER_NOT_FOUND, [
            { message: [ErrorMessageKey.USER_NOT_FOUND], property: 'username' },
          ]);

        // compare password
        const isPassword = comparePassword(body.password, userCheck.password);
        if (!isPassword)
          throw new InputValidationError(ErrorMessageKey.PASSWORD_INCORRECT, [
            { message: [ErrorMessageKey.PASSWORD_INCORRECT], property: 'password' },
          ]);

        // if use sign up by cms system will check role
        if (systemType === SystemType.CMS_SYSTEM) {
          // only accept with role supper admin and admin
          if (userCheck.role === RoleType.END_USER)
            throw new UnauthorizedError(ErrorMessageKey.PERMISSION_DENIED, [
              { message: [ErrorMessageKey.PERMISSION_DENIED], property: 'username' },
            ]);
        }

        // generate token
        const tokenPayload: ITokenContent = {
          id: userCheck.id,
        };

        tokenInfo = await this.createToken(tokenPayload);
      });
      return tokenInfo;
    } catch (error) {
      throw error;
    }
  }

  /**
   * register user into system
   * @param body
   * @returns UserEntity
   */
  async signUp(body: ISignUpReq): Promise<UserEntity> {
    let signUpRes: UserEntity | null = null;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        if (!body.username)
          throw new InputValidationError(ErrorMessageKey.USERNAME_REQUIRED, [
            { message: [ErrorMessageKey.USERNAME_REQUIRED], property: 'username' },
          ]);
        if (!body.email)
          throw new InputValidationError(ErrorMessageKey.EMAIL_REQUIRED, [
            { message: [ErrorMessageKey.EMAIL_REQUIRED], property: 'email' },
          ]);
        if (!body.password)
          throw new InputValidationError(ErrorMessageKey.PASSWORD_REQUIRED, [
            { message: [ErrorMessageKey.PASSWORD_REQUIRED], property: 'password' },
          ]);

        // check user valid
        const userCheckEmail = await manager.findOne(UserEntity, {
          where: [{ username: body.username }, { email: body.email }],
        });

        if (userCheckEmail)
          throw new InputValidationError(ErrorMessageKey.USERNAME_OR_EMAIL_VALID, [
            { message: [ErrorMessageKey.USERNAME_OR_EMAIL_VALID], property: 'username' },
            { message: [ErrorMessageKey.USERNAME_OR_EMAIL_VALID], property: 'email' },
          ]);

        const newAccount = manager.create(UserEntity, {
          username: body.username,
          email: body.email,
          password: body.password,
          role: body.role ? body.role : RoleType.END_USER,
        });
        const createdAccount = await manager.save(newAccount);
        signUpRes = createdAccount;
      });
      return signUpRes;
    } catch (error) {
      throw error;
    }
  }

  /**
   * create response token
   * @param tokenPayload ITokenContent
   * @returns ITokenInfo
   */
  async createToken(tokenPayload: ITokenContent): Promise<ITokenInfo> {
    // access token
    const accessToken = await this.authenticationServices.generateToken(
      tokenPayload,
      parseInt(this.configService.get('JWT_ACCESS_TOKEN_EXPIRES'), 10),
    );
    // refresh token
    const refreshToken = await this.authenticationServices.generateToken(
      tokenPayload,
      parseInt(this.configService.get('JWT_REFRESH_TOKEN_EXPIRES'), 10),
    );

    const loginRes: ITokenInfo = {
      accessToken: accessToken,
      refreshToken: refreshToken,
      expireTime: parseInt(this.configService.get('JWT_ACCESS_TOKEN_EXPIRES'), 10),
    };
    return loginRes;
  }
}
