import { ITokenInfo } from '@common/interfaces';
import { UserEntity } from '@entities';
import { Body, Controller, HttpCode, HttpStatus, Inject, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { responseHelper } from '@utils/helper';
import { Logger } from 'winston';
import { AccountService } from './account.service';
import { ISignInReq, ISignUpReq } from './dtos/account.req';

@ApiTags('Account')
@Controller('account')
export class AccountController {
  constructor(
    private readonly accountService: AccountService,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   * user login system
   * @param body ISignInReq
   * @returns ITokenInfo
   */
  @ApiBody({ type: ISignInReq })
  @ApiOperation({
    operationId: 'sign-in',
    summary: 'User can login system',
    description: 'User can retrieve access token',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
    description: 'Happy case: User login successfully',
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
    description: 'The input params does not correct',
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Cannot find the account in system',
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
    description: 'The input params does not correct',
  })
  @ApiResponse({
    type: ITokenInfo,
  })
  @Post('/sign-in')
  @HttpCode(200)
  async signIn(@Body() body: ISignInReq) {
    const signInRes = await this.accountService.signIn(body);
    return responseHelper<ITokenInfo>(signInRes);
  }

  /**
   *
   * @param body ISignUpReq
   * @returns UserEntity
   */
  @ApiBody({ type: ISignUpReq })
  @ApiOperation({
    operationId: 'sign-up',
    summary: 'User can register system',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: UserEntity,
  })
  @Post('/sign-up')
  @HttpCode(200)
  async signUp(@Body() body: ISignUpReq) {
    const signUpRes = await this.accountService.signUp(body);
    return responseHelper<UserEntity>(signUpRes);
  }
}
