import { Module } from '@nestjs/common';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';
import { AuthenticationModule } from '../authentication/authentication.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [AuthenticationModule, ConfigModule],
  controllers: [AccountController],
  providers: [AccountService],
  exports: [],
})
export class AccountModule {}
