import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtAuthenticationGuard } from 'src/guard';
import { AuthenticationService } from './authentication.service';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    ConfigModule,
    JwtModule.register({}),
    PassportModule.register({
      defaultStrategy: ['jwt'],
      property: 'account',
      session: false,
    }),
  ],
  providers: [AuthenticationService, JwtStrategy, JwtAuthenticationGuard],
  exports: [PassportModule, AuthenticationService],
})
export class AuthenticationModule {}
