import { ITokenContent } from '@common/interfaces';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthenticationService {
  constructor(private configService: ConfigService, private jwtService: JwtService) {}

  /**
   * generate a token
   * @param payload
   * @param expireTime
   * @returns
   */
  async generateToken(payload: ITokenContent, expireTime: number): Promise<string> {
    return this.jwtService.signAsync(payload, {
      expiresIn: expireTime,
      secret: this.configService.get('JWT_SECRET_KEY'),
    });
  }

  /**
   * when expired token then will error
   * @param token
   * @returns
   */
  async verifyToken(token: string): Promise<ITokenContent> {
    try {
      return this.jwtService.verifyAsync(token, {
        secret: this.configService.get('JWT_SECRET_KEY'),
      });
    } catch (error) {
      throw error;
    }
  }
}
