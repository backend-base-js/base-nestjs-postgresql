import { AccountInfo } from '@common/decorators';
import { IAccountInfo } from '@common/interfaces';
import { UserEntity } from '@entities';
import { JwtAuthenticationGuard } from '@guard';
import { Controller, Get, HttpCode, HttpStatus, Inject, Query, UseGuards } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { responseHelper } from '@utils/helper';
import { Logger } from 'winston';
import { IFindUserReq } from './dtos/user.req';
import { UserService } from './user.service';

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   *
   * @param params IFindUserReq
   * @returns IListUserRes
   */
  @ApiBearerAuth()
  @ApiParam({
    name: 'limit',
    type: 'number',
    required: false,
  })
  @ApiParam({
    name: 'page',
    type: 'number',
    required: false,
  })
  @ApiParam({
    name: 'email',
    type: 'string',
    required: false,
  })
  @ApiOperation({
    operationId: 'get list user',
    description: 'Get list user registered in system',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: [UserEntity],
  })
  @Get('/list')
  @HttpCode(200)
  @UseGuards(JwtAuthenticationGuard)
  async getUsers(@Query() params?: IFindUserReq) {
    const users = await this.userService.findUser(params);
    return responseHelper<UserEntity[]>(users.data, { total: users?.total });
  }

  /**
   *
   * @param
   * @returns UserEntity
   */
  @ApiBearerAuth()
  @ApiOperation({
    operationId: 'get current user',
    description: 'Get information of current user logging system',
  })
  @ApiOkResponse({
    status: HttpStatus.OK,
  })
  @ApiBadRequestResponse({
    status: HttpStatus.BAD_REQUEST,
  })
  @ApiNotFoundResponse({
    status: HttpStatus.NOT_FOUND,
  })
  @ApiUnprocessableEntityResponse({
    status: HttpStatus.UNPROCESSABLE_ENTITY,
  })
  @ApiResponse({
    type: UserEntity,
  })
  @Get('/current')
  @HttpCode(200)
  @UseGuards(JwtAuthenticationGuard)
  async getCurrentUserInfo(@AccountInfo() accountInfo: IAccountInfo) {
    const user = await this.userService.findUserById(accountInfo?.id);
    return responseHelper<UserEntity>(user);
  }
}
