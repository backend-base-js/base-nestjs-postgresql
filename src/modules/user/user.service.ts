import { OrderType } from '@common/enums';
import { NotFoundError } from '@common/errors';
import { ErrorMessageKey } from '@common/errors/messages';
import { UserEntity } from '@entities';
import { Inject, Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import { Connection, EntityManager } from 'typeorm';
import { Logger } from 'winston';
import { IFindUserReq } from './dtos/user.req';
import { IListUserRes } from './dtos/user.res';

@Injectable()
export class UserService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    @Inject('winston') private readonly logger: Logger,
  ) {}

  /**
   *
   * @param params IFindUserReq
   * @returns IListUserRes
   */
  async findUser(params: IFindUserReq): Promise<IListUserRes> {
    let responseData: IListUserRes | null = null;
    try {
      const limit = params?.limit;
      const page = params.page - 1;

      await this.connection.transaction(async (manager: EntityManager) => {
        // get total record
        const totalRecord = await manager
          .getRepository(UserEntity)
          .createQueryBuilder('user')
          .where('user.email = :email', { email: params.email })
          .getCount();

        // query data
        const $queries = manager
          .getRepository(UserEntity)
          .createQueryBuilder('user')
          .where('user.email = :email', { email: params.email });

        if (limit) {
          $queries.take(limit);
        }
        if (page) {
          $queries.skip(page * limit);
        }

        const listUser = await $queries.orderBy('user.createdAt', OrderType.DESC).getMany();
        responseData = {
          data: listUser,
          total: totalRecord,
        };
      });
      return responseData;
    } catch (error) {
      throw error;
    }
  }

  /**
   *
   * @param id number
   * @returns UserEntity
   */
  async findUserById(id: number): Promise<UserEntity> {
    let userData: UserEntity | null = null;
    try {
      await this.connection.transaction(async (manager: EntityManager) => {
        const user = await manager.findOne(UserEntity, { where: { id } });
        if (!user) throw new NotFoundError(ErrorMessageKey.USER_NOT_FOUND);
        userData = user;
      });
      return userData;
    } catch (error) {
      throw error;
    }
  }
}
