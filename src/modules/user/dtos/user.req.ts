import { IParamsCommonReq } from '@common/interfaces';
import { ApiProperty } from '@nestjs/swagger';

export class IFindUserReq extends IParamsCommonReq {
  @ApiProperty()
  email?: string;
}
