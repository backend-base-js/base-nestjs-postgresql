import { UserEntity } from '@entities';
import { ApiProperty } from '@nestjs/swagger';

export class IListUserRes {
  @ApiProperty({ required: true })
  data?: UserEntity[];

  @ApiProperty({ required: true })
  total?: number;
}
