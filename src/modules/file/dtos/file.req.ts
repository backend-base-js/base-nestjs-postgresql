import { IParamsCommonReq } from '@common/interfaces';
import { ApiProperty } from '@nestjs/swagger';

export class CreateOneFileArgs {
  @ApiProperty({ type: String })
  url: string;

  @ApiProperty({ type: String })
  publicId: string;

  @ApiProperty({ nullable: true })
  metadata?: Record<string, any>;
}

export class UpdateOneFileArgs {
  @ApiProperty({ type: String })
  url: string;

  @ApiProperty({ type: String })
  publicId: string;

  @ApiProperty({ nullable: true })
  metadata?: Record<string, any>;
}

export class DeleteOneFileArgs {
  @ApiProperty({ type: String })
  url?: string;

  @ApiProperty({ type: String })
  publicId: string;

  @ApiProperty({ nullable: true })
  metadata?: Record<string, any>;
}

export class FindFileArgs extends IParamsCommonReq {
  @ApiProperty({ nullable: true })
  publicId?: string;
}
