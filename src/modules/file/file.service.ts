import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';
import { FileEntity } from '../../entities/file.entity';
import { CreateOneFileArgs, DeleteOneFileArgs, UpdateOneFileArgs } from './dtos/file.req';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(FileEntity)
    private readonly fileRepository: Repository<FileEntity>,
  ) {}

  async create(args: CreateOneFileArgs) {
    const newFile = this.fileRepository.create(args);
    await this.fileRepository.insert(newFile);
    return newFile;
  }

  findFirst(args: FindOneOptions<FileEntity>) {
    return this.fileRepository.findOne(args);
  }

  async update(args: UpdateOneFileArgs) {
    return await this.fileRepository.update({ publicId: args.publicId }, { ...args });
  }

  async delete(args: DeleteOneFileArgs) {
    return this.fileRepository.delete({ ...args });
  }
}
