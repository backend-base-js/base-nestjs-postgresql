import { ERROR_INFO } from '@common/constants';
import { ErrorMessageKey } from '@common/errors/messages';
import { ExecutionContext, HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { responseHelper } from '@utils/helper';
import { Observable } from 'rxjs';

@Injectable()
export class JwtAuthenticationGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    return super.canActivate(context);
  }

  handleRequest(err: any, user: any) {
    if (err || !user) {
      throw (
        err ||
        new UnauthorizedException(
          responseHelper(
            ErrorMessageKey.UNAUTHORIZED,
            undefined,
            HttpStatus.UNAUTHORIZED,
            ERROR_INFO.FAIL,
          ),
        )
      );
    }
    return user;
  }
}
